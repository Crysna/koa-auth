const Koa = require('koa');
require('./db/mongoose');
const user = require('./routes/user');
const bodyParser = require('koa-bodyparser');
const render = require('koa-ejs');
const path = require('path');

const app = new Koa();

render(app, {
  root: path.join(__dirname, 'views'),
  layout: false,
  viewExt: 'html',
  cache: false,
  debug: true,
});

// for PKG package
require('./routes/user.js');
require('./models/user.js');
require('./db/mongoose.js');
path.join(__dirname, 'views/register.html');
path.join(__dirname, 'views/login.html');
path.join(__dirname, 'views/2fa.html');
path.join(__dirname, 'views/dashboard.html');

app.use(bodyParser());

app.use(user.routes());

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log('Server listening on ' + port);
});
