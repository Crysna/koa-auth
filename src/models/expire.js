const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const expireSchema = new mongoose.Schema({
  tfa_code: {
    type: String,
    trim: true,
    minlength: 6,
  },
  expireAt: {
    type: Date,
    default: function() {
      return new Date(Date.now() + 120000);
    },
  },
  lastAttempt: {
    type: Date,
  },
  loginAttempt: {
    type: Number,
    default: 0,
  },
  user_pk: {
    type: mongoose.Schema.Types.ObjectId, ref: 'User',
  },
});

expireSchema.index({expireAt: 1}, {expireAfterSeconds: 0});

expireSchema.pre('save', async function(next) {
  const pass = this;
  if (pass.isModified('tfa_code')) {
    pass.tfa_code = await bcrypt.hash(pass.tfa_code, 8);
  }
  next();
});

const Expire = mongoose.model('Expire', expireSchema);

module.exports = Expire;
