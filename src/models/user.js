const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    unique: true,
  },
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true,
    lowercase: true,
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error('Email is invalid!');
      }
    },
  },
  password: {
    type: String,
    required: true,
    trim: true,
    minlength: 7,
    validate(value) {
      if (value.toLowerCase().includes('password')) {
        throw new Error('Password cannot contain "password"');
      }
    },
  },
  created_at: {
    type: Date,
    default: Date.now,
  },
  lockedAt: {
    type: Date,
  },
  lockStatus: {
    type: Number,
  },
});

// userSchema.statics.findByOpt = async (email, code, ctx) => {
//   const optCode = await User.findOne({email});
//   const optMatch = await bcrypt.compare(code, optCode.otp.tfa_code);
//   if (!optMatch) {
//     ctx.body = 'OTP Code does not match!';
//   }
//   return optCode;
// };

// userSchema.statics.findByCredentials = async (email, pswd) => {
//   const user = await User.findOne({email});
//   if (!user) {
//     throw new Error('Unable to login!');
//   }
//   const isMatch = await bcrypt.compare(pswd, user.password);
//   if (!isMatch) {
//     throw new Error('Unable to login!');
//   }
//   return user;
// };

userSchema.pre('save', async function(next) {
  const user = this;
  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

const User = mongoose.model('User', userSchema);

module.exports = User;
