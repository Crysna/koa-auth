const Router = require('koa-router');
const nodemailer = require('nodemailer');
const User = require('../models/user');
const Expire = require('../models/expire');
const bcrypt = require('bcryptjs');

const router = new Router();

router.get('', async (ctx) => {
  await ctx.render('register');
});

router.get('/login', async (ctx) => {
  await ctx.render('login', {message: ''});
});

router.post('/users', async (ctx) => {
  const user = new User(ctx.request.body);
  try {
    await user.save();
    ctx.status = 201;
    ctx.redirect('/login');
  } catch (e) {
    ctx.status = 400;
    ctx.body = 'Ups, Something went wrong!';
  }
});

router.post('/verify', async (ctx) => {
  const email = await ctx.request.body.email;
  const code = await ctx.request.body.code;
  try {
    const optCode = await User.findOne({email});
    const idMatch = await Expire.findOne({user_pk: optCode._id});
    const optMatch = await bcrypt.compare(code, idMatch.tfa_code);
    const MAX_LOGIN_ATTEMPT = 2;
    await Expire.findByIdAndUpdate(idMatch, {
      lastAttempt: new Date().getTime() + 10000,
    });
    // eslint-disable-next-line max-len
    if (optMatch && Date.now() < idMatch.lastAttempt || !optMatch && Date.now() < idMatch.lastAttempt) {
      return ctx.render('2fa', {
        // eslint-disable-next-line max-len
        message: 'you need at least 10 seconds interval between each login attempts!',
        email: optCode.email,
      });
    }
    await Expire.findByIdAndUpdate(idMatch, {$inc: {loginAttempt: 1}});
    if (!optMatch && idMatch.loginAttempt >= MAX_LOGIN_ATTEMPT) {
      await Expire.findByIdAndDelete(idMatch);
      await User.findByIdAndUpdate(optCode._id, {
        lockedAt: new Date,
        lockStatus: 1,
      });
      return ctx.render('login', {message: 'Account has been locked!'});
    }
    if (!optMatch && idMatch.loginAttempt < MAX_LOGIN_ATTEMPT) {
      return ctx.render('2fa', {
        message: 'OTP code is incorrect!',
        email: optCode.email,
      });
    }
    if (optMatch && idMatch.loginAttempt <= 3) {
      await Expire.findByIdAndDelete(idMatch);
      return ctx.render('dashboard', {
        message: 'Authentication Successfully!',
      });
    }
  } catch (e) {
    // Expire.find({exist: {$exists: false}})
    const userId = await User.findOne({email});
    const exist = await Expire.findOne({user_pk: userId._id});
    if (!exist) {
      return ctx.render('2fa', {
        message: 'OTP code was expired!',
        email: userId.email,
      });
    } else {
      ctx.status = 400;
      ctx.body = 'Ups, Something went wrong!';
    }
  }
});

router.post('/resend', async (ctx) => {
  const email = await ctx.request.body.email;
  try {
    const doc = await User.findOne({email});
    const otp = await Math.floor(Math.random() * 900000) + 100000;
    const match = await Expire.findOne({user_pk: doc._id});
    await Expire.findByIdAndDelete(match);
    const exp = new Expire({
      user_pk: doc._id,
      tfa_code: otp,
    });
    try {
      await exp.save();
      ctx.status = 201;
    } catch (error) {
      ctx.status = 400;
      ctx.body = 'Ups, Something went wrong!';
    }
    if (!doc) {
      ctx.status = 404;
    }

    const transporter = await nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'please provide an email service here',
        pass: 'email password here',
      },
    });
    const mailOptions = await {
      from: 'provide an email here',
      to: doc.email,
      subject: '2FA Password',
      // eslint-disable-next-line max-len
      html: '<p>OTP Code:<b> ' + otp + '</b></p>',
    };
    await transporter.sendMail(mailOptions, (err, ctx) => {
      if (err) {
        ctx.err = 400;
        ctx.body = 'Ups, Something went wrong!';
      } else {
        ctx.status = 200;
        ctx.body = 'OTP has been sent. Please cek your email';
      }
    });
    await ctx.render('2fa', {
      email: doc.email,
      message: 'New OTP code has been sent to your Email.',
    });
  } catch (err) {
    ctx.status = 400;
    ctx.body = 'Ups, Something went wrong!';
  }
});

router.post('/login', async (ctx) => {
  const email = await ctx.request.body.email;
  const pswd = await ctx.request.body.password;
  try {
    const user = await User.findOne({email});
    if (!user) {
      return ctx.render('login', {message: 'Invalid Email or Password!'});
    }
    const isMatch = await bcrypt.compare(pswd, user.password);
    if (!isMatch) {
      return ctx.render('login', {message: 'Invalid Email or Password!'});
    }
    const doc = await User.findOne({email});
    const otp = await Math.floor(Math.random() * 900000) + 100000;
    if (doc.lockStatus == 1) {
      await ctx.render('login', {message: 'Account has been locked!'});
      // ctx.body = {message: 'Account has been locked!'};
    } else {
      const match = await Expire.findOne({user_pk: doc._id});
      await Expire.findByIdAndDelete(match);
      const exp = new Expire({
        user_pk: doc._id,
        tfa_code: otp,
      });
      try {
        await exp.save();
        ctx.status = 201;
      } catch (error) {
        ctx.status = 400;
        ctx.body = 'Ups, Something went wrong!';
      }
      if (!doc) {
        ctx.status = 404;
      }

      const transporter = await nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'please provide an email servie here',
          pass: 'email password here',
        },
      });
      const mailOptions = await {
        from: 'provide an email here',
        to: doc.email,
        subject: '2FA Password',
        // eslint-disable-next-line max-len
        html: '<p>OTP Code:<b> ' + otp + '</b></p>',
      };
      await transporter.sendMail(mailOptions, (err, ctx) => {
        if (err) {
          ctx.err = 400;
          ctx.body = 'Ups, Something went wrong!';
        } else {
          ctx.status = 200;
          ctx.body = 'OTP has been sent. Please cek your email';
        }
      });
      await ctx.render('2fa', {
        email: doc.email,
        message: 'Login Successfully',
      });
    }
  } catch (e) {
    ctx.status = 400;
    ctx.body = 'Ups, Something went wrong!';
  }
});

module.exports = router;
